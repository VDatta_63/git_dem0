
/**
* @brief Header file
* @file example1.h
 * @author Vinay
 * @version 0.1
 * @date 2024-06-14
 * @copyright Copyright (c) 2024

*/


extern void swap(int *a, int *b);