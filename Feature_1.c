/**
 * @file example1.c
 * @author Vinay
 * @brief 
 * @version 0.1
 * @date 2024-06-14
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include <stdio.h>
#include<examplefile1.h>

/**
 * @brief 
 * 
 * @param a 
 * @param b 
 */
extern void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
    return;
}



